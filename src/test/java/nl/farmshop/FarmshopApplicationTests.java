package nl.farmshop;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FarmshopApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void happyFlowShouldGiveBackFlock() throws Exception {
		String flockJson = restTemplate.getForObject("http://localhost:" + port + "/farmshop/v1/flock",
				String.class);
		assertEquals("{\"flock\":[{\"type\":\"goat\",\"name\":\"Alice\",\"sex\":\"f\",\"milk\":50},{\"type\":\"goat\",\"name\":\"Stanislas\",\"sex\":\"m\",\"milk\":50},{\"type\":\"sheep\",\"name\":\"Robert\",\"sex\":\"m\",\"milk\":30,\"wool\":34},{\"type\":\"sheep\",\"name\":\"Betty\",\"sex\":\"f\",\"milk\":30,\"wool\":28},{\"type\":\"lamb\",\"name\":\"Teemo\",\"sex\":\"m\",\"wool\":5}]}", flockJson);
	}
}
