package nl.farmshop.importer;

import nl.farmshop.domain.Animal;
import nl.farmshop.domain.Lamb;
import nl.farmshop.domain.Sex;
import nl.farmshop.domain.Sheep;
import nl.farmshop.service.FlockStockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class FlockXmlImporterTest {

    private FlockXmlImporter flockXmlImporter;
    @Mock
    private FlockStockService flockStockService;

    @Test
    public void importOnlySheeps() throws IOException {
        // Given
        List<Animal> animals = Arrays.asList(new Sheep("Robert", Sex.MALE, 34)
                                    , new Sheep("Betty", Sex.FEMALE, 28));
        flockXmlImporter = new FlockXmlImporter(flockStockService, "imports/import-sheeps.xml");
        // When
        flockXmlImporter.importFlock();
        // Then
        verify(flockStockService, times(1)).insertFlock(animals);
    }

    @Test
    public void importOneLamb() throws IOException {
        // Given
        List<Animal> animals = Arrays.asList(new Lamb("Teemo", Sex.MALE, 5));
        flockXmlImporter = new FlockXmlImporter(flockStockService, "imports/import-lambs.xml");
        // When
        flockXmlImporter.importFlock();
        // Then
        verify(flockStockService, times(1)).insertFlock(animals);
    }

    @Test
    public void emptyImportShouldWork() throws IOException {
        // Given
        List<Animal> animals= Arrays.asList();
        flockXmlImporter = new FlockXmlImporter(flockStockService, "imports/import-empty.xml");
        // When
        flockXmlImporter.importFlock();
        // Then
        verify(flockStockService, times(1)).insertFlock(animals);
    }





}