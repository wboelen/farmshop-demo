package nl.farmshop.service;

import nl.farmshop.domain.Animal;
import nl.farmshop.domain.Goat;
import nl.farmshop.domain.Lamb;
import nl.farmshop.domain.Sex;
import nl.farmshop.domain.Sheep;
import nl.farmshop.exception.InsufficientStockException;
import nl.farmshop.exception.UnknownProductException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class FlockStockServiceTest {

    private FlockStockService flockStockService;

    @Before
    public void startUp() {
        flockStockService = new FlockStockService();
    }

    @Test
    public void fullStock() {
        // Given
        List<Animal> animals = completeFlock();
        // When
        flockStockService.insertFlock(animals);
        // Then
        assertEquals(Integer.valueOf(160), flockStockService.getStockLevelProducts().get("milk"));
        assertEquals(Integer.valueOf(91), flockStockService.getStockLevelProducts().get("wool"));
    }

    @Test
    public void noStock() {
        // Given
        // When
        flockStockService.insertFlock(new ArrayList<>());
        // Then
        assertEquals(Integer.valueOf(0), flockStockService.getStockLevelProducts().get("milk"));
        assertEquals(Integer.valueOf(0), flockStockService.getStockLevelProducts().get("wool"));
    }

    @Test
    public void processOrderShouldDecreaseStockLevels() throws Exception {
        // Before
        flockStockService.insertFlock(completeFlock());
        // Given
        Map<String, Integer> orderItems = new HashMap<>();
        orderItems.put("milk", 15);
        orderItems.put("wool", 25);
        // When
        flockStockService.processOrderItems(orderItems);
        // Then
        assertEquals(Integer.valueOf(145), flockStockService.getStockLevelProducts().get("milk"));
        assertEquals(Integer.valueOf(66), flockStockService.getStockLevelProducts().get("wool"));
    }

    @Test(expected = InsufficientStockException.class)
    public void tooLowStockShouldThrowInsufficientStockException() throws Exception {
        // Before
        flockStockService.insertFlock(Arrays.asList(new Sheep("blackie", Sex.FEMALE, 10),
                new Sheep("blackie2", Sex.MALE, 20)));
        // Given
        Map<String, Integer> orderItems = new HashMap<>();
        orderItems.put("milk", 45);
        // When
        flockStockService.processOrderItems(orderItems);
    }

    @Test(expected = UnknownProductException.class)
    public void unknownProductkShouldThrowUnknownProductException() throws Exception {
        // Before
        flockStockService.insertFlock(Arrays.asList(new Sheep("blackie", Sex.FEMALE, 10),
                new Sheep("blackie2", Sex.MALE, 20)));
        // Given
        Map<String, Integer> orderItems = new HashMap<>();
        orderItems.put("milk", 45);
        orderItems.put("egg", 45);
        // When
        flockStockService.processOrderItems(orderItems);
    }

    private List<Animal> completeFlock() {
        return Arrays.asList(new Sheep("blackie", Sex.FEMALE, 10),
                new Sheep("blackie2", Sex.MALE, 20),
                new Sheep("blackie3", Sex.FEMALE, 1),
                new Sheep("blackie4", Sex.MALE, 20),
                new Goat("Gotie", Sex.MALE),
                new Goat("Gotie2", Sex.FEMALE),
                new Goat("Gotie3", Sex.MALE),
                new Goat("Gotie4", Sex.FEMALE),
                new Lamb("lambie", Sex.MALE, 20),
                new Lamb("lambie2", Sex.FEMALE, 20));
    }

}