package nl.farmshop.controller;

import nl.farmshop.domain.Animal;
import nl.farmshop.domain.Lamb;
import nl.farmshop.domain.Sex;
import nl.farmshop.exception.InsufficientStockException;
import nl.farmshop.exception.UnknownProductException;
import nl.farmshop.importer.FlockXmlImporter;
import nl.farmshop.service.FlockStockService;

import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@WebMvcTest(FarmShopController.class)
public class FarmshopControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FlockStockService flockStockService;

    @MockBean
    private FlockXmlImporter flockXmlImporter;

    @Test
    public void getFlockShouldReturnFlock() throws Exception {
        List<Animal> animals = Arrays.asList(new Lamb("Teemo", Sex.MALE, 5));
        when(flockStockService.getFlock()).thenReturn(animals);
        this.mockMvc.perform(get("/farmshop/v1/flock"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalToIgnoringCase("{\"flock\":[{\"type\":\"lamb\",\"name\":\"Teemo\",\"sex\":\"m\",\"wool\":5}]}")));
    }

    @Test
    public void getStockShouldReturnAvailableStock() throws Exception {
        Map<String, Integer> stockLevels = new HashMap<>();
        stockLevels.put("milk", 0);
        stockLevels.put("wool", 5);
        when(flockStockService.getStockLevelProducts()).thenReturn(stockLevels);
        this.mockMvc.perform(get("/farmshop/v1/stock"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalToIgnoringCase("{\"wool\":5,\"milk\":0}")));
    }

    @Test
    public void processOrder() throws Exception {
        Map<String, Integer> stockLevels = new HashMap<>();
        stockLevels.put("milk", 0);
        stockLevels.put("wool", 5);
        when(flockStockService.getStockLevelProducts()).thenReturn(stockLevels);
        this.mockMvc.perform(post("/farmshop/v1/order")
                .contentType(MediaType.APPLICATION_JSON).content("{\"customer\":\"Janssen\",\"order\":{\"orderItems\":[{\"product\":\"wool\",\"quantityRequested\":8},{\"product\":\"milk\",\"quantityRequested\":10}]}}"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalToIgnoringCase("Thank you Janssen for placing your order at the farmshop")));
    }

    @Test
    public void unKnownProductShouldGiveBackStatus404() throws Exception {
        doThrow(new UnknownProductException("")).when(flockStockService).processOrderItems(any());
        this.mockMvc.perform(post("/farmshop/v1/order")
                .contentType(MediaType.APPLICATION_JSON).content("{\"customer\":\"Janssen\",\"order\":{\"orderItems\":[{\"product\":\"egg\",\"quantityRequested\":8}]}}"))
                .andExpect(status().is(404));
    }

    @Test
    public void insufficientStockhouldGiveBackStatus500() throws Exception {
        doThrow(new InsufficientStockException("")).when(flockStockService).processOrderItems(any());
        this.mockMvc.perform(post("/farmshop/v1/order")
                .contentType(MediaType.APPLICATION_JSON).content("{\"customer\":\"Janssen\",\"order\":{\"orderItems\":[{\"product\":\"egg\",\"quantityRequested\":8}]}}"))
                .andExpect(status().is(500));
    }
}