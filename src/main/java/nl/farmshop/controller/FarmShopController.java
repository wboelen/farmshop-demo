package nl.farmshop.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.farmshop.domain.Animal;
import nl.farmshop.exception.InsufficientStockException;
import nl.farmshop.exception.UnknownProductException;
import nl.farmshop.service.FlockStockService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@RestController()
@RequestMapping("/farmshop/v1")
public class FarmShopController {

    private final FlockStockService flockStockService;

    public FarmShopController(FlockStockService flockStockService) {
        this.flockStockService = flockStockService;
    }

    @GetMapping(path = "/flock")
    public FlockJson getFlock() {
        return new FlockJson(flockStockService.getFlock());
    }

    @GetMapping(path = "/stock")
    public Map<String, Integer> getStocklevels() {
        return flockStockService.getStockLevelProducts();
    }
    
    @PostMapping(path = "/order")
    public String doOrder(@Valid @RequestBody CustomerOrder customerOrder) throws InsufficientStockException, UnknownProductException {
        Map<String, Integer> orderItems = customerOrder.getOrder().getOrderItems().stream()
                .collect(toMap(i -> i.getProduct(), i -> i.quantityRequested));
        flockStockService.processOrderItems(orderItems);
        return String.format("Thank you %s for placing your order at the farmshop", customerOrder.getCustomer());
    }

    /*
    Json helper classes to deserialize xml/json to object. Alternatively you could use an xsd
    */

    @AllArgsConstructor
    @Getter
    public static class FlockJson {
        @JsonProperty("flock")
        List<Animal> animals;
    }

    @Getter
    public static class CustomerOrder {
        @NotNull
        private String customer;
        @NotNull
        private Order order;
    }

    @Getter
    public static class Order {
        @NotNull
        private List<OrderItem> orderItems;
    }

    @Getter
    public static class OrderItem {
        @NotNull
        private String product;
        @NotNull
        private int quantityRequested;
    }
}
