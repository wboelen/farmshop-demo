package nl.farmshop.importer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import nl.farmshop.domain.Goat;
import nl.farmshop.domain.Sheep;
import nl.farmshop.service.FlockStockService;
import nl.farmshop.domain.Animal;
import nl.farmshop.domain.Lamb;
import nl.farmshop.domain.Sex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FlockXmlImporter {

    private final FlockStockService flockStockService;
    private final String fileName;

    public FlockXmlImporter(FlockStockService flockStockService, @Value("${farmshop.import.filename}") String fileName) {
        this.flockStockService = flockStockService;
        this.fileName = fileName;
    }

    public void importFlock() throws IOException {

        FlockJson flockJson = objectMapper().readValue(getResourceFromFileName(), FlockJson.class);

        List<Animal> animals = new ArrayList<>();
        animals.addAll(
            flockJson.getGoatJsons().stream().map(g -> new Goat(g.getName(), g.getSex())).collect(Collectors.toList())
        );
        animals.addAll(
                flockJson.getSheepJson().stream().map(s -> new Sheep(s.getName(), s.getSex(), s.getWool())).collect(Collectors.toList())
        );
        animals.addAll(
                flockJson.getLambJsons().stream().map(l -> new Lamb(l.getName(), l.getSex(), l.getWool())).collect(Collectors.toList())
        );

        flockStockService.insertFlock(animals);
    }

    private InputStream getResourceFromFileName() {
        return getClass().getClassLoader().getResourceAsStream(fileName);
    }

    private static ObjectMapper objectMapper() {
        JacksonXmlModule xmlModule = new JacksonXmlModule();
        xmlModule.setDefaultUseWrapper(false);
        ObjectMapper objectMapper = new XmlMapper(xmlModule);
        return objectMapper;
    }

    /*
    Json helper classes to deserialize xml/json to object. Alternatively you could use an xsd
     */

    @Getter
    @JsonRootName("flock")
    private static class FlockJson {
        @JsonProperty("sheep")
        List<SheepJson> sheepJson = new ArrayList<>();
        @JsonProperty("goat")
        List<GoatJson> goatJsons = new ArrayList<>();
        @JsonProperty("lamb")
        List<LambJson> lambJsons = new ArrayList<>();
    }

    @Getter
    private static class SheepJson {
        private String name;
        private Sex sex;
        private int wool;
    }

    @Getter
    private static class GoatJson {
        private String name;
        private Sex sex;
    }

    @Getter
    private static class LambJson {
        private String name;
        private Sex sex;
        private int wool;
    }


}
