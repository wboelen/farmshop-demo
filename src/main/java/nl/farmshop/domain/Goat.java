package nl.farmshop.domain;

import lombok.Value;

@Value
public class Goat extends Animal implements ProducesMilk {

    private int milk = 50;

    public Goat(String name, Sex gender) {
        super(name, gender);
    }

    @Override
    public int getMilk() {
        return milk;
    }

    @Override
    public boolean canProduceMilk() {
        return this.sex == Sex.FEMALE;
    }
}
