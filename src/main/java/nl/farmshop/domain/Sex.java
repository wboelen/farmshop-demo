package nl.farmshop.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Sex {
    MALE("m"),
    FEMALE("f");

    @JsonValue
    private String shortName;

    Sex(String shortName) {
        this.shortName = shortName;
    }
}
