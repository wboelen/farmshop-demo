package nl.farmshop.domain;

import lombok.Value;

@Value
public class Sheep extends Animal implements ProducesMilk, ProducesWool {

    private int milk = 30;
    private int wool;

    public Sheep(String name, Sex gender, int wool) {
        super(name, gender);
        this.wool = wool;
    }

    @Override
    public int getMilk() {
        return milk;
    }

    @Override
    public boolean canProduceMilk() {
        return this.sex == Sex.FEMALE;
    }

    @Override
    public int getWool() {
        return wool;
    }
}
