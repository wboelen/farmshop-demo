package nl.farmshop.domain;

import lombok.Value;

@Value
public class Lamb extends Animal implements ProducesWool {

    private int wool;

    public Lamb(String name, Sex gender, int wool) {
        super(name, gender);
        this.wool = wool;
    }

    @Override
    public int getWool() {
        return wool;
    }
}
