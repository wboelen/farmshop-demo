package nl.farmshop.domain;

public interface ProducesWool {

    int getWool();
}
