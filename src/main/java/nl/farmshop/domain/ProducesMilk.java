package nl.farmshop.domain;

public interface ProducesMilk {

    int getMilk();

    boolean canProduceMilk();
}
