package nl.farmshop.service;

import lombok.extern.slf4j.Slf4j;
import nl.farmshop.domain.ProducesWool;
import nl.farmshop.exception.InsufficientStockException;
import nl.farmshop.exception.UnknownProductException;
import nl.farmshop.domain.Animal;
import nl.farmshop.domain.ProducesMilk;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;

@Component
@Slf4j
public class FlockStockService {

    private static final ToIntFunction<Animal> TO_MILK = a -> ((ProducesMilk) a).getMilk();
    private static final ToIntFunction<Animal> TO_WOOL = a -> ((ProducesWool) a).getWool();
    private static final Predicate<Animal> PRODUCES_MILK = a -> a instanceof ProducesMilk;
    private static final Predicate<Animal> CAN_PRODUCE_MILK = a -> ((ProducesMilk) a).canProduceMilk();
    private static final Predicate<Animal> PRODUCES_WOOL = a -> a instanceof ProducesWool;

    private ConcurrentHashMap<String, Integer> productsOnStock = new ConcurrentHashMap<>();
    private List<Animal> animals = new ArrayList();

    public void insertFlock(List<Animal> animals) {

        int milk = animals.stream().filter(PRODUCES_MILK.and(CAN_PRODUCE_MILK)).mapToInt(TO_MILK).sum();
        int wool = animals.stream().filter(PRODUCES_WOOL).mapToInt(TO_WOOL).sum();

        this.animals = animals;
        this.productsOnStock.put("milk", milk);
        this.productsOnStock.put("wool", wool);
    }

    public void processOrderItems(Map<String, Integer> orderItems) throws InsufficientStockException, UnknownProductException {
        checkIfProductsAreAvailable(orderItems);
        checkStockLevels(orderItems);

        for (Map.Entry<String, Integer> entry : orderItems.entrySet()) {
            int result = productsOnStock.compute(entry.getKey(), (p, s) -> s - entry.getValue());
            log.info("New stock level for {} is {}", entry.getKey(), result);
            if (result < 0) {
                log.error("Oeps something went very wrong. Result {} for stock {} too low with input {}", result, entry.getKey(), entry.getValue());
                throw new RuntimeException("Oeps something went very wrong");
            }
        }
    }

    private void checkIfProductsAreAvailable(Map<String, Integer> orderItems) throws UnknownProductException {
        for (String product : orderItems.keySet()) {
            if (!productsOnStock.containsKey(product)) {
                log.info(String.format("Product %s is not on stock", product));
                throw new UnknownProductException(String.format("Product %s is not on stock", product));
            }
        }
    }

    private void checkStockLevels(Map<String, Integer> orderItems) throws InsufficientStockException {
        for (Map.Entry<String, Integer> entry : orderItems.entrySet()) {
            if (entry.getValue() > productsOnStock.get(entry.getKey())) {
                log.info(String.format("Product %s has insufficient stock for amount %s", entry.getKey(), entry.getValue()));
                throw new InsufficientStockException(String.format("Product %s has insufficientstock. Stock left %s", entry.getKey(), productsOnStock.get(entry.getKey())));
            }
        }
    }

    public Map<String, Integer> getStockLevelProducts() {
        return productsOnStock;
    }

    public List<Animal> getFlock() {
        return animals;
    }
}
