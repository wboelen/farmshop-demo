package nl.farmshop;

import nl.farmshop.importer.FlockXmlImporter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FarmshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(FarmshopApplication.class, args);
	}

	@Autowired
	private FlockXmlImporter flockXmlImporter;

	@Bean
	public CommandLineRunner startup() {
        return args -> flockXmlImporter.importFlock();
    }
}
