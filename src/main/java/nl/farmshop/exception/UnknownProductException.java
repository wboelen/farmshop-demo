package nl.farmshop.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UnknownProductException extends Exception {

    public UnknownProductException(String message) {
        super(message);
    }
}
