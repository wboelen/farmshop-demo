# farmshop

To start the shop run _mvn spring-boot:run_

Endpoints can be tested with curl  
_curl http://localhost:7474/farmshop/v1/flock_     
_curl http://localhost:7474/farmshop/v1/stock_     
_curl -X POST http://localhost:7474/farmshop/v1/order -H "Content-Type: application/json" -d '{"customer":"Janssen","order":{"orderItems":[{"product":"wool","quantityRequested":8},{"product":"milk","quantityRequested":10}]}}'_

